from django.urls import path, include

from placeholder import views

from .views import mainView

urlpatterns = [
    path('bulakhovs-thoughts/', views.LatestBulakhovsThoughtsView.as_view()),
    path('bulakhovs-single-thought/', views.BulakhovsThoughtView.as_view()),
    path('bulakhovs-thoughts-count/', views.ThoughtsCountView.as_view()),
    path('bulakhovs-news/', views.LatestPlaceholderNewsView.as_view()),
    path('', views.mainView, name='mainView'),
]