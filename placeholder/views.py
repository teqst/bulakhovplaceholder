from django.db.models import Q
from rest_framework.decorators import api_view
from django.shortcuts import render, Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic import ListView, DetailView, View
from .models import PlaceholderText, PlaceholderNews
from .serializers import PlaceholderTextSerializer, PlaceholderNewsSerializer

import random
# Create your views here.


def mainView(request):
    return render(request, 'placeholder/main.html', {})


class LatestBulakhovsThoughtsView(APIView):
    def get(self, request, format=None):
        thoughts = PlaceholderText.objects.all()[0:4]
        serializer = PlaceholderTextSerializer(thoughts, many=True)
        return Response(serializer.data)


class BulakhovsThoughtView(APIView):
    def get(self, request, format=None):
        item = list(PlaceholderText.objects.all())
        random_thought = random.choice(item)
        serializer = PlaceholderTextSerializer(random_thought)
        return Response(serializer.data)


class ThoughtsCountView(APIView):
    def get(self, request, format=None):
        items = PlaceholderText.objects.count()
        content = {'thoughts_count': items}
        return Response(content)


class LatestPlaceholderNewsView(APIView):
    def get(self, request, format=None):
        news = PlaceholderNews.objects.all()[0:20]
        serializer = PlaceholderNewsSerializer(news, many=True)
        return Response(serializer.data)