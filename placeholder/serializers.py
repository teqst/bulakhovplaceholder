from rest_framework import serializers

from .models import PlaceholderText, PlaceholderNews


class PlaceholderTextSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlaceholderText
        fields = (
            'id',
            'name',
            'text',
            'topic',
            'date_added'
        )


class PlaceholderNewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlaceholderNews
        fields = (
            'id',
            'name',
            'text',
            'topic',
            'get_image',
            'get_audio',
            'get_video',
            'get_thumbnail',
            'date_added',
        )
