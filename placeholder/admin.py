from django.contrib import admin
from .models import PlaceholderText, PlaceholderNews

# Register your models here.

admin.site.register(PlaceholderText)

admin.site.register(PlaceholderNews)