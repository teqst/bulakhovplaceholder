const news_list = document.querySelector('.news__list');
const try_to_load_span = document.querySelector('.try_to_load');

const run_code_text = document.querySelector('.button--run-code--only-text');
const request_field = document.querySelector('.code__text--only-text-result');

const run_code_media = document.querySelector('.button--run-code--media');

let result_text = '';
let result_media = '';


//Functions set

function createNewsCard(item) {
    const card = document.createElement("div");
    card.setAttribute('class', 'news-card')
    card.innerHTML = `
        <h1 class="news-card__title" alt="${item.date_added}">${item.name}</h1>
        <p class="news-card__description">${item.text}</p>
        <video src="${item.get_video}" controls class="news-card__video"></video>
        <audio
            class="news-card__audio"
            controls
            src="${item.get_audio}">
                Your browser does not support the
                <code>audio</code> element.
        </audio>
    `;
    news_list.append(card);
}

const getNews = function () {
   fetch('http://127.0.0.1:8000/api/v1/bulakhovs-news/')
       .then(result => result.json())
       .then(json => {
           console.log(json)
           result_media = json;
           try_to_load_span.hidden = true;
           for(let item of json) {
               createNewsCard(item);
           }
           run_code_media.removeEventListener('click', getNews, false);
           run_code_media.setAttribute('disabled', 'disabled');
           run_code_media.classList.add('button--disabled');
       })
}

//Listeners

run_code_text.addEventListener('click', ()=> {
   fetch('https://artemka711.pythonanywhere.com/api/v1/bulakhovs-single-thought/')
       .then(result => result.json())
       .then(json => {
           console.log(json)
           result_text = JSON.stringify(json, null, 2);
           request_field.innerText = result_text
       })
})


run_code_media.addEventListener('click', getNews, false)
